package week7.exercise5Solutions;

import java.util.Scanner;
public class Ex5Q13ValidateInteger
{
    
    public static void main(String[] args)
    {
//    Testing Q9
        Scanner keyboard = new Scanner(System.in);
       int num1;
       int num2;
       
        System.out.print("Please enter first number:");
        num1 = keyboard.nextInt();
        
        num2 = getValidInt(3, 8, "Please enter second numeber ");
        
    
    }
    
    public static int getValidInt(int min, int max, String prompt)
    {
        
        Scanner keyboard = new Scanner(System.in);  
        int value;   
        
        System.out.print(prompt);         
        value = keyboard.nextInt(); 
        
        while (value < min || value > max)      // Note condition. Also consider do/while?
        {         
            System.out.println("Error - integer value not in correct range");  // improve message
            
            System.out.print(prompt);         
            value = keyboard.nextInt();             
        }   
        return value;
        
    }
}