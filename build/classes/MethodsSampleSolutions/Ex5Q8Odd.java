package week7.exercise5Solutions;

import java.util.Scanner;

public class Ex5Q8Odd  //(** Example of BOOLEAN method **)
{

    public static void main(String[] args)
    {
        //Testing Q8
        Scanner keyboard = new Scanner(System.in);
        boolean ans;
        int value;

        ans = isOdd(5);
        System.out.println(ans);
        
        
        System.out.println("Please enter number:");
        value = keyboard.nextInt();
        ans = isOdd(value);
        if (ans == true)
        {
            System.out.println(value + " is an odd number");
        } 
        else
        {
            System.out.println(value + " is an even number");
        }
        
        
        System.out.println("Please enter number:");
        value = keyboard.nextInt();
        if (isOdd(value))
        {
            System.out.println("number is odd");
        } 
        else
        {
            System.out.println("number is even");
        }

    }

    //Q8
    public static boolean isOdd(int num)
    {
//    Version#1     
//        boolean result;           // Alternatively ... (i)no variable (ii) no if
//        if (num % 2 == 1)
//        {
//            result =  true;
//        }
//        else
//        {
//            result = false;    
//        } 
        

//    Version#2            
//        result = num % 2 == 1;
//        return result;
        
        
//    Version#3  
        return num % 2 == 1;

    }

}
