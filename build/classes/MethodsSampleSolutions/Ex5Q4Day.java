package week7.exercise5Solutions;

import java.util.Scanner;

public class Ex5Q4Day
{
    public static void main(String[] args) 
    {
        Scanner keyboard = new Scanner(System.in); 
        
        System.out.println("Please enter number:");
        int dayNumber = keyboard.nextInt();
                
        String dayName = getDayName(dayNumber);
        System.out.println("The day you chose was" + dayName);       
        
    }    
    
    public static String getDayName(int dayNum)
    {  
        String result = ""; // LOCAL variable
        
        if (dayNum == 1)
        {
            result = "Monday";
        }
        else if (dayNum == 2)
        {
            result = "Tuesday";
        }
        else if (dayNum == 3)
        {
            result = "Wednesday";
        }
        else if (dayNum == 4)
        {
            result = "Thursday";
        }
        else if (dayNum == 5)
        {
            result = "Friday";
        }
        else if (dayNum == 6)
        {
            result = "Saturday";
        }
        else 
        {
            result = "Sunday";
        }
        
        return result;
    }
}


