package week7.exercise5Solutions;

import java.util.Scanner;

public class Ex5Q2Area
{
    public static void main(String[] args) 
    {
        Scanner keyboard = new Scanner(System.in); 
        
        double circleArea = calcArea(2.45);
        System.out.println("Area = " + circleArea); 
        
    } 
    
    public static double calcArea(double radius)
    {
        double area; // Local variable
        
        area = Math.PI * Math.pow(radius, 2);        
        
        return area;       
    }       
    
}
