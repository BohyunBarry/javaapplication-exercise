
package week7.exercise5Solutions;


public class Ex5Q14TempConversion
{

    public static void main(String[] args)
    {
        System.out.println("\tCels. Temp\tFahr. Temp");
        System.out.println("\t--------------------------");
        for (int celsius = 0; celsius <= 100; celsius += 10)
        {
            System.out.printf("%12d%18.2f\n", celsius, convertToFahr(celsius));
        }

    }

    public static double convertToFahr(int temp)
    {
        return (9.0 / 5) * temp + 32;
    }

}
