
package week7.exercise5Solutions;

import java.util.Random;

public class Ex5Q7RandomDay
{ 
    
    public static void main(String[] args)
    {
        
        String magicDay;
        magicDay = getRandomDay();
        System.out.println(magicDay);
    }

    public static String getRandomDay()
    {
        Random rand = new Random();
       
        int num = 1 + rand.nextInt(7);
        String day;  // local
        day = getDayName(num);  // Re-use of method from Q4 ******
        
        return day;

    }
    
    public static String getDayName(int dayNum)
    {  
        String result; 
        
        if (dayNum == 1)
        {
            result = "Monday";
        }
        else if (dayNum == 2)
        {
            result = "Tuesday";
        }
        else if (dayNum == 3)
        {
            result = "Wednesday";
        }
        else if (dayNum == 4)
        {
            result = "Thursday";
        }
        else if (dayNum == 5)
        {
            result = "Friday";
        }
        else if (dayNum == 6)
        {
            result = "Saturday";
        }
        else 
        {
            result = "Sunday";
        }
        
        return result;
    }

}
