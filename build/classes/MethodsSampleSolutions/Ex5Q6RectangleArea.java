package week7.exercise5Solutions;

import java.util.*;

public class Ex5Q6RectangleArea 
{
    public static void main(String[] args) 
    {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Program to calculate the area of a rectangle ...");
        System.out.println();
        System.out.print("Enter the length : ");
        double length = keyboard.nextDouble();
        System.out.print("Enter width : ");
        double width = keyboard.nextDouble();
        
        double area = getArea(length, width);
        System.out.printf("Area of a rectangle %.2f by %.2f = %.2f\n", length, width, area);
    }
    
    public static double getArea(double l, double w) 
//  Note - parameters & arguments do NOT have to have the same names.
    {
        return l * w;
    }
}
