package week7.exercise5Solutions;

import java.util.Scanner;

public class Ex5Q12SumRange
{

    public static void main(String[] args)
    {
//    Testing Q12
        Scanner keyboard = new Scanner(System.in);
        int result;
        int num1;
        int num2;
        
        
        System.out.print("Please enter first number:");
        num1 = keyboard.nextInt();
        System.out.print("Please enter second number:");
        num2 = keyboard.nextInt();

        result = sumRange(num1, num2);
        System.out.println("Sum of numbers ion the range = " + result);

    }

  
    public static int sumRange(int value1, int value2)
    {
        int sum = 0;
        for (int i = value1; i <= value2; i++)
        {
            sum = sum + i;
        }
        return sum;
    }

}
