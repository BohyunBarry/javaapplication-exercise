package MethodsSampleSolutions;

import java.util.Scanner;

public class Ex5Q9Multiple  //(** Example of BOOLEAN method **)
{

    public static void main(String[] args)
    {
//    Testing Q9
        Scanner keyboard = new Scanner(System.in);
       
        System.out.print("Please enter first number:");
        int num1 = keyboard.nextInt();
        System.out.print("Please enter second number:");
        int num2 = keyboard.nextInt();
    
        if (isMultiple(num1, num2))
        {
            System.out.println(num2 + " IS a multiple of " + num1);
        } 
        else
        {
            System.out.println(num2 + " is a NOT multiple of " + num1);
        }

    }

    //Q9
    public static boolean isMultiple(int value1, int value2)
    {
        return value2 % value1 == 0;

    }

}
