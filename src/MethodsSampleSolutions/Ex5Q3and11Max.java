package MethodsSampleSolutions;

//  Shows method examples for max() overloaded 
//  Could also call the Math.max() method - look in the java API ****

import java.util.Scanner;

public class Ex5Q3and11Max
{

    public static void main(String[] args)
    {
        Scanner keyboard = new Scanner(System.in);
        int ans2 = findMax(23, 45, 56);
        System.out.println("The max is " + ans2);

//        System.out.println("Please enter number:");
//        int num1 = keyboard.nextInt();
//
//        System.out.println("Please enter number:");
//        int num2 = keyboard.nextInt();
//
//        System.out.println("Please enter number:");
//        int num3 = keyboard.nextInt();
//
//        int ans = findMax(num1, num2, num3);
//        System.out.println("The max is " + ans);
        
         double ans3 = findMax(23.5, 45.70, 56.4);

    }

    public static int findMax(int num1, int num2, int num3) //overloaded below
    {
        int result; // Local variable
        if (num1 >= num2 && num1 >= num3)
        {
            result = num1;
        } 
        else if (num2 >= num1 && num2 >= num3)
        {
            result = num2;
        } 
        else
        {
            result = num3;
        }
        return result;
    }

    public static int findMax(int num1, int num2) //2 int parameters
    {
        int result;  // Local variable

        if (num1 > num2)
        {
            result = num1;
        } 
        else
        {
            result = num2;
        }

        return result;
    }

    public static double findMax(double num1, double num2, double num3)
    {
        double result;
        if (num1 >= num2 && num1 >= num3)
        {
            result = num1;
        } 
        else if (num2 >= num1 && num2 >= num3)
        {
            result = num2;
        } 
        else //if (num2 >= num1 && num3 >= num2)  // not neded
        {
            result = num3;
        }
        return result;
    }

}
