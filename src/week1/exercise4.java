/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week1;
import java.util.Scanner;
/**
 *
 * @author d00152394
 */
public class exercise4 {
    public static void main(String[] args)
    {        
        
        Scanner kb = new Scanner(System.in);
        
        double radius;
        double area;
        double volume;
        double length;
        
        System.out.print("enter radius = ");
        radius = kb.nextDouble();
        
        System.out.print("enter length = ");
        length = kb.nextDouble();
        
        area = radius * radius * Math.PI;
        //Math.PI = 3.141592653589793 = 22/7
        volume = area * length;
        
        System.out.println("The area for the circle of radius " + radius + " is " + area);
        System.out.println("The volume for the circle is " + volume);
        
        
    }
}
