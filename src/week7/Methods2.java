/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week7;


/**
 *
 * @author d00152394
 */
public class Methods2
{
     public static void main(String[] args)
    {
        
        double area = calculateArea(5); 
                
        System.out.printf("Area = %.2f%n",area);
          
    }
     
     public static double calculateArea(double radius) 
    {
        double calculateArea;
        
        calculateArea = (Math.PI) * radius * radius; //Math.PI * Math.pow(radius,2)
  
        return calculateArea;
    }
}
