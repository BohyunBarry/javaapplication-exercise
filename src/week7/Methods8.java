/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week7;
import java.util.Scanner;
/**
 *
 * @author d00152394
 */
public class Methods8 
{
   
    public static void main(String[] args)
    {
        Scanner kb = new Scanner(System.in);
        boolean input; 
        int num;
        
        System.out.print("Please enter a number:");
        num = kb.nextInt();
        
        input = odd(num);
        
        System.out.println(input);
    }
    
    
    public static boolean odd(int number)
    {
         boolean ans;
        
        if(number%2 == 1)
        {
            ans = true;
        }
        else
        {
            ans = false;
        }
       
        return ans;
        
    }
}
