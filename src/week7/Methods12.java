/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week7;


import java.util.Scanner;

/**
 *
 * @author user
 */
public class Methods12 {
    public static void main(String[]args)
    { 
        Scanner kb = new Scanner(System.in);
          
        int num1;
        int num2;
        int total;
        
        System.out.print("Please enter first number:");
        num1 = kb.nextInt();
        System.out.print("Please enter second number:");
        num2 = kb.nextInt();
        
        total = sum(num1,num2);
            
        System.out.println(total);
        
    }
    public static int sum(int num1,int num2)
    {
        int sum = 0;
        
        for(int i = num1 ; i <= num2 ; i++)
        {
            sum = i + sum;
        }
        return sum;
    }
    
}
