/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week7;

/**
 *
 * @author d00152394
 */
public class Methods3
{
    public static void main(String[] args)
    {
        int ans;
        ans = getMax(2,4,6);                
        System.out.println("the Max number = " + ans);
        
        ans = getMax(3,6,9);                
        System.out.println("the Max number = " + ans);
          
    }
     
     public static int getMax(int num1 , int num2 , int num3) 
    {
       int ans ;
       if(num1>num2 && num1>num3)
       {
           ans = num1;
       }
       else if(num2>num3 && num2>num1)
       {
           ans = num2;
       }
       else
       {
           ans = num3;
       }
                
        return ans;
    }
}
