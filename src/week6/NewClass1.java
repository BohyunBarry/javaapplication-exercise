/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week6;

/**
 *
 * @author d00152394
 */
public class NewClass1 {
    
 public static void main(String[] args) {
        egg();
        teaCup();
        stopSign();
        hat();
        house();
       
    }

    
    public static void eggTop() {
        System.out.println("  ______");
        System.out.println(" /      \\");
        System.out.println("/        \\");
    }
    
    public static void eggBottom() {
        System.out.println("\\        /");
        System.out.println(" \\______/");
    }
    
    public static void egg() {
        eggTop();
        eggBottom();
        System.out.println();
    }

    
    public static void teaCup() {
        eggBottom();
        line();
        System.out.println();
    }
    

    public static void stopSign() {
        eggTop();
        System.out.println("|  STOP  |");
        eggBottom();
        System.out.println();
    }
    

    public static void hat() {
        eggTop();
        line();
        System.out.println();
    }
    
    public static void house() {
        eggTop();
        line();
        line();
        line();
        line();
    }


    public static void line() {
        System.out.println("+--------+");
    }

    
}
