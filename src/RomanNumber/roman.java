 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week9;


import java.util.Scanner;

/**
 *
 * @author user
 */
public class roman {
    public static void main(String[] args)
    {        
        Scanner kb = new Scanner(System.in);
        
        String[]roman={"\nerror","I","II","III","IV","V","VI","VII","VIII","IX","X","XI","XII","XIII","XIV","XV","XVI","XVII","XVIII","XIX","XX","XXI","XXII","XXIII","XXIV","XXV","XXVI","XXVII","XXVIII","XXIX","XXX","XXXI","XXXII","XXXIII","XXXIV","XXXV","XXXVI","XXXVII","XXXVIII","XXXIX","XL","XLI","XLII","XLIII","XLIV","XLV","XLVI","XLVII","XLVIII","XLIX","L","LI","LII","LIII","LIV","LV","LVI","LVII","LVIII","LIX","LX","LXI","LXII","LXIII","LXIV","LXV","LXVI","LXVII","LXVIII","LXIX","LXX","LXXI","LXXII","LXXIII","LXXIV","LXXV","LXXVI","LXXVII","LXXVIII","LXXIX","LXXX","LXXXI","LXXXII","LXXXIII","LXXXIV","LXXXV","LXXXVI","LXXXVII","LXXXVIII","LXXXIX","XC","XCI","XCII","XCIII","XCIV","XCV","XCVI","XCVII","XCVIII","XCIX"};
        int num;
        String romanNum;
        String check;
        int check2=0;
        
        System.out.print("Do you wish to enter a (D) decimal number, (R) roman numeral or (Q) to quit: " );
        check = kb.next();
        do
        {
        if("D".equals(check))
        {
            System.out.print("please enter a number = ");
            num = kb.nextInt();
                        
            if(num<roman.length)
            {
                System.out.println(roman[num] + "\n");
            } 
            else if(num<200)
            {
                num=num-100;
                System.out.println("C" + roman[num] + "\n");
            }
            else if(num<300)
            {
                num=num-200;
                System.out.println("CC" + roman[num] + "\n");
            }
            else if(num<400)
            {
                num=num-300;
                System.out.println("CCC" + roman[num] + "\n");
            }
            else if(num<500)
            {
                num=num-400;
                System.out.println("CD" + roman[num] + "\n");
            }
            else if(num<600)
            {
                num=num-500;
                System.out.println("D" + roman[num] + "\n");
            }
            else if(num<700)
            {
                num=num-600;
                System.out.println("DC" + roman[num] + "\n");
            }
            else if(num<800)
            {
                num=num-700;
                System.out.println("DCC" + roman[num] + "\n");
            }
            else if(num<900)
            {
                num=num-800;
                System.out.println("DCCC" + roman[num] + "\n");
            }
            else if(num<1000)
            {
                num=num-900;
                System.out.println("CM" + roman[num] + "\n");
            }
            else if(num<1100)
            {
                num=num-1000;
                System.out.println("M" + roman[num] + "\n");
            }
            else if(num<1200)
            {
                num=num-1100;
                System.out.println("MM" + roman[num] + "\n");
            }
            else if(num<1300)
            {
                num=num-1200;
                System.out.println("MMM" + roman[num] + "\n");
            }
            
            else
            {
                System.out.println("\nerror\n");
            }
                      
        }
        if("R".equals(check)) 
        {
            System.out.print("pls enter a roman = ");
            romanNum = kb.next();
            
            for(int i=1;i<=roman.length-1;i++)
            {               
                if(romanNum.equals(roman[i]))
                {
                    System.out.println(i + "\n");
                    check2=1;
                }
            }
            
            if(check2!=1)
            {
                System.out.println("\nerror\n");
            }
            check2=0;
            
        }
        System.out.print("Do you wish to enter a (D) decimal number, (R) roman numeral or (Q) to quit: " );
        check = kb.next();
        }while(!"Q".equals(check));    
        
        System.out.print("\nThank you for using this program");
        
               
    }
}
