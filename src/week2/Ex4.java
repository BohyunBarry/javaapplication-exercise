/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week2;

import java.util.Scanner;

/**public class Ex4
{

    public static void main(String[] args)
    {
 *
 * @author d00152394
 */
public class Ex4
{

    public static void main(String[] args)
    {

        Scanner kb = new Scanner(System.in);

        int number;
        int type;
        int logo;
        final double CP_PRICE = 12.50;
        final double SS_PRICE = 15.50;
        final double LS_PRICE = 18.50;
        final double logo_PRICE = 1;
        double subtotal;
        double total;
        int countLogo = 0;
        int count;
        double allTotal = 0;
        double max = 0;

        System.out.print("enter the number for order = ");
        count = kb.nextInt();

        for (int i = 1; i <= count; i++)
        {
            System.out.print("enter the number do you want to buy = ");
            number = kb.nextInt();

            System.out.print("enter a number (1)Cotton (2)Short Sleeve (3)Long Sleeve  the jersey you choice = ");
            type = kb.nextInt();

            System.out.print("you want have a logo? (yes=1 / no=2) = ");
            logo = kb.nextInt();

            if (type == 1)
            {
                subtotal = (number * CP_PRICE);
            } 
            else if (type == 2)
            {
                subtotal = (number * SS_PRICE);
            } 
            else
            {
                subtotal = (number * LS_PRICE);
            }

            if (logo == 1)
            {
                total = subtotal + (number * logo_PRICE);
                countLogo = countLogo + 1;
            } 
            else
            {
                total = subtotal;
            }

            if (type < 1 || type > 3 || logo < 0 || logo > 2)
            {
                System.out.println("sorry, deal with user input values outside valid ranges ");
            } else
            {
                System.out.println("\nenter number of jerseys : " + number);
                System.out.println("jersey type : (1)Cotton (2)Short Sleeve (3)Long Sleeve ");
                System.out.println("enter jersey type(123) : " + type);
                System.out.println("logo?(1)yes (2)no : " + logo);
                System.out.println("the total cost for these jerseys is € " + total);
                System.out.println("\n");

                //(i)
                if (total >= 250)
                {
                    total = total - (total * 0.05);
                }

                //(ii)
                if (type == 1 && logo == 1)
                {
                    System.out.println("sorry, the logo on cotton jerseys takes longer than the normal print run");
                }

                //(iii)
                if (total < 100)
                {
                    System.out.println("sorry, your order is too small, the minimum order cost is €100");
                }

                //(iv)
                System.out.printf("the final total cost is €%.2f", total);
                System.out.println("\n\n\n");

                
                
                allTotal = allTotal + total;
                
                if(total > max)
                {
                    max = total;
                }
                
                
            
                
            }
            

        }
        
                System.out.printf("the all total cost is €%.2f", allTotal);
                System.out.println("\n");
                
             
                System.out.printf("max = " + max);
                System.out.println("\n");

                
                System.out.printf("count logo = " + countLogo);
                System.out.println("\n\n");
    }
}
