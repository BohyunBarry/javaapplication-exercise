/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week9;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class CA 
{

    public static void main(String[] args) throws IOException 
    {
        File inputFile = new File("CA.txt");
        Scanner input = new Scanner(inputFile);

        Random rand = new Random();
        Scanner kb = new Scanner(System.in);

        int random1;
        int random2;
        int question1;
        int question2;
        int question3;
        int topic;
        int type;
        int lv;
        double question4;
        double ans2;
        int ans;
        int mark = 0;
        int check;

        System.out.println("Welcome to play this game,please choose you want to play with the numbers generated either randomly OR read from a file.");
        System.out.print("* choose 1) Maths Questions from File  2) Random Maths Question (with points)  = ");
        topic = kb.nextInt();

        if (topic == 1) 
        {

            int number1;
            int number2;
            int number3;
            int number4;
            int number5;
            int number6;
            int number7;
            int number8;
            int answer1;
            int answer2;
            int answer3;
            double answer4;
            int solution1;
            int solution2;
            int solution3;
            double solution4;
            int grownLv = 0;
            double grownMark = 2.5;
            int totalcount = 0;
            int count1 = 0;
            int count2 = 0;
            int count3 = 0;
            int count4 = 0;
            int countMark;
            int countDistance =0;
           
            
            System.out.println("\n\n");

            System.out.println("********** ATTENTION!!! **********");

            System.out.println(" now start the game!!!" + "\n");
            
            while (input.hasNext()) 
            {

                number1 = input.nextInt();
                number2 = input.nextInt();
                number3 = input.nextInt();
                number4 = input.nextInt();
                number5 = input.nextInt();
                number6 = input.nextInt();
                number7 = input.nextInt();
                number8 = input.nextInt();

                grownLv = grownLv + 1;
                System.out.println("\n*****LV " + grownLv + "*****");

                solution1 = number1 + number2;
                System.out.print(number1 + " + " + number2 + " = ");
                answer1 = kb.nextInt();

                solution2 = number3 - number4;
                System.out.print(number3 + " - " + number4 + " = ");
                answer2 = kb.nextInt();

                solution3 = number5 * number6;
                System.out.print(number5 + " * " + number6 + " = ");
                answer3 = kb.nextInt();

                solution4 = (double) number7 / number8;
                System.out.print(number7 + " / " + number8 + " = ");
                answer4 = kb.nextInt();

                grownMark = grownMark * 2;

                if (solution1 == answer1) 
                {
                    count1 = (int) (count1 + grownMark);
                }
                if (solution2 == answer2)
                {
                    count2 = (int) (count2 + grownMark);
                }
                if (solution3 == answer3) 
                {
                    count3 = (int) (count3 + grownMark);
                }
                if (solution4 == answer4) 
                {
                    count4 = (int) (count4 + grownMark);
                }

                totalcount = count1 + count2 + count3 + count4;
                countMark = totalcount - countDistance;
                
                System.out.println("your got " + countMark + " mark for this LV");
                countDistance = totalcount;
                
            }
            System.out.println("\n\n----- your total mark = " + totalcount + " -----");

        } 
        
        else
            
        {

            System.out.println("\nThere are three levels of difficulty: 1, 2, or 3");
            System.out.print("* choose  the LV (1 , 2 , 3) = ");
            lv = kb.nextInt();

            System.out.println("\nThere are four types of questions: (1)addition, (2)subtraction, (3)multiplication or (4)division");
            System.out.print("* choose  the type (1=+ , 2=- , 3=* , 4=/) = ");
            type = kb.nextInt();

            System.out.println("\n\n");

            System.out.println("********** ATTENTION!!! **********");

            System.out.println(" now start the game!!!" + "\n");

            if (lv == 1)
            {
                if (type == 1)
                {
                    do 
                    {
                        random1 = 1 + rand.nextInt(12);
                        random2 = 1 + rand.nextInt(12);

                        System.out.print(random1 + " + " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question1 = getquestion1(random1, random2);

                        if (ans == question1)
                        {
                            mark = mark + 5;
                            System.out.println("--- your answer is correct, you can get the 5 point ---");
                        } 
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1)
                        {
                            System.out.println("\n");
                        }

                        if (check == 2)
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                } 
                
                else if ((type == 2)) 
                {
                    do 
                    {
                        random1 = 1 + rand.nextInt(12);
                        random2 = 1 + rand.nextInt(12);

                        System.out.print(random1 + " - " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question2 = getquestion2(random1, random2);

                        if (ans == question2)
                        {
                            mark = mark + 5;
                            System.out.println("--- your answer is correct, you can get the 5 point ---");
                        } 
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }
                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                } 
                
                else if ((type == 3)) 
                    
                {
                    do 
                    {
                        random1 = 1 + rand.nextInt(12);
                        random2 = 1 + rand.nextInt(12);

                        System.out.print(random1 + " * " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question3 = getquestion3(random1, random2);

                        if (ans == question3)
                        {
                            mark = mark + 5;
                            System.out.println("--- your answer is correct, you can get the 5 point ---");
                        } 
                        else
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1)
                        {
                            System.out.println("\n");
                        }

                        if (check == 2)
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                } 
                else 
                {
                    do
                    {
                        random1 = 1 + rand.nextInt(12);
                        random2 = 1 + rand.nextInt(12);

                        System.out.print(random1 + " / " + random2);

                        System.out.print(" entre the answer = ");
                        ans2 = kb.nextDouble();

                        System.out.println();

                        question4 = getquestion4(random1, random2);

                        if (ans2 == question4) 
                        {
                            mark = mark + 5;
                            System.out.println("--- your answer is correct, you can get the 5 point ---");
                        } 
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1)
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);

                }

            }
            
            else if (lv == 2)
                
            {
                if (type == 1)
                {
                    
                    do 
                    {
                        random1 = 12 + rand.nextInt(39);
                        random2 = 12 + rand.nextInt(39);

                        System.out.print(random1 + " + " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question1 = getquestion1(random1, random2);

                        if (ans == question1) 
                        {
                            mark = mark + 10;
                            System.out.println("--- your answer is correct, you can get the 10 point ---");
                        } 
                        
                        else
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                }
                
                else if ((type == 2))
                {
                    do 
                    {
                        random1 = 12 + rand.nextInt(39);
                        random2 = 12 + rand.nextInt(39);

                        System.out.print(random1 + " - " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question2 = getquestion2(random1, random2);

                        if (ans == question2)
                        {
                            mark = mark + 10;
                            System.out.println("--- your answer is correct, you can get the 10 point ---");
                        }
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                } 
                
                else if ((type == 3))
                {
                    do
                    {
                        random1 = 12 + rand.nextInt(39);
                        random2 = 12 + rand.nextInt(39);

                        System.out.print(random1 + " * " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question3 = getquestion3(random1, random2);

                        if (ans == question3)
                        {
                            mark = mark + 10;
                            System.out.println("--- your answer is correct, you can get the 10 point ---");
                        }
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1)
                        {
                            System.out.println("\n");
                        }

                        if (check == 2)
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                } 
                
                else 
                    
                {
                    do
                    {
                        random1 = 12 + rand.nextInt(39);
                        random2 = 12 + rand.nextInt(39);

                        System.out.print(random1 + " / " + random2);

                        System.out.print(" entre the answer = ");
                        ans2 = kb.nextDouble();

                        System.out.println();

                        question4 = getquestion4(random1, random2);

                        if (ans2 == question4) 
                        {
                            mark = mark + 10;
                            System.out.println("--- your answer is correct, you can get the 10 point ---");
                        }
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);

                }
            } 
            else
            {
                if (type == 1) 
                {
                    do 
                    {
                        random1 = 50 + rand.nextInt(51);
                        random2 = 50 + rand.nextInt(51);

                        System.out.print(random1 + " + " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question1 = getquestion1(random1, random2);

                        if (ans == question1)
                        {
                            mark = mark + 20;
                            System.out.println("--- your answer is correct, you can get the 20 point ---");
                        }
                        else
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2)
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                } 
                
                else if ((type == 2))
                {
                    do 
                    {
                        random1 = 50 + rand.nextInt(51);
                        random2 = 50 + rand.nextInt(51);

                        System.out.print(random1 + " - " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question2 = getquestion2(random1, random2);

                        if (ans == question2) 
                        {
                            mark = mark + 20;
                            System.out.println("--- your answer is correct, you can get the 20 point ---");
                        } 
                        else
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1)
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                } 
                else if ((type == 3)) 
                {
                    do
                    {
                        random1 = 50 + rand.nextInt(51);
                        random2 = 50 + rand.nextInt(51);

                        System.out.print(random1 + " * " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question3 = getquestion3(random1, random2);

                        if (ans == question3)
                        {
                            mark = mark + 20;
                            System.out.println("--- your answer is correct, you can get the 20 point ---");
                        } 
                        else
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                }
                else 
                {
                    do 
                    {
                        random1 = 50 + rand.nextInt(51);
                        random2 = 50 + rand.nextInt(51);

                        System.out.print(random1 + " / " + random2);

                        System.out.print(" entre the answer = ");
                        ans2 = kb.nextDouble();

                        System.out.println();

                        question4 = getquestion4(random1, random2);

                        if (ans2 == question4) 
                        {
                            mark = mark + 20;
                            System.out.println("--- your answer is correct, you can get the 20 point ---");
                        }
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);

                }
            }

            System.out.println("\n\n***** thank you for playing this game *****");
            System.out.println("***** your this Lv total mark is " + mark + " *****");

        }
    }

    public static int getquestion1(int num1, int num2) 
    {
        int question1;
        question1 = num1 + num2;

        return question1;
    }

    public static int getquestion2(int num1, int num2) 
    {
        int question2;
        question2 = num1 - num2;

        return question2;
    }

    public static int getquestion3(int num1, int num2) 
    {
        int question3;
        question3 = num1 * num2;

        return question3;
    }

    public static double getquestion4(int num1, int num2) 
    {
        double question4;
        question4 = (double) num1 / num2;

        return question4;
    }

}
