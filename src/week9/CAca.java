/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week9;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class CAca 
{
     public static void main(String[] args) throws IOException 
    {
        Scanner kb = new Scanner(System.in);
        int topic;
        
        System.out.println("Welcome to play this game,please choose you want to play with the numbers generated either randomly OR read from a file.");
        System.out.print("* choose 1) Maths Questions from File  2) Random Maths Question (with points)  = ");
        topic = kb.nextInt();
        while(topic!=3)
        {
            if(topic==1)
            {
                playGameFileData();
            }
            else
            {
                playGameRandom();
            }
            
            System.out.print("\n* choose 1) Maths Questions from File  2) Random Maths Question (with points) 3) Finish = ");
            topic = kb.nextInt();
        }
        
        System.out.println("\n\n***** thank you for playing this game *****");
        
    }

      public static void playGameFileData() throws IOException 
    {
        File inputFile = new File("tryca.txt");
        Scanner input = new Scanner(inputFile);
        Scanner kb = new Scanner(System.in);

            String question;
            int answer;
            int solution;
            int mark=0;
                      
            System.out.println("\n********** ATTENTION!!! **********");
            System.out.println(" now start the game!!!" + "\n");
            
            while (input.hasNext()) 
            {
                 question = input.next();
                 answer = input.nextInt();
               
                System.out.print(question);
                solution = kb.nextInt();

                if (solution == answer) 
                {
                    mark=mark+5;
                }                                
            }
            System.out.println("\n\n----- your total mark = " + mark + " -----");

        } 
        
        public static void playGameRandom()             
        {
            Random rand = new Random();
            Scanner kb = new Scanner(System.in);

        int random1;
        int random2;
        int question1;
        int question2;
        int question3;
        int topic;
        int type;
        int lv;
        double question4;
        double ans2;
        int ans;
        int mark = 0;
        int check;
            
            System.out.println("\nThere are three levels of difficulty: 1, 2, or 3");
            System.out.print("* choose  the LV (1 , 2 , 3) = ");
            lv = kb.nextInt();

            System.out.println("\nThere are four types of questions: (1)addition, (2)subtraction, (3)multiplication or (4)division");
            System.out.print("* choose  the type (1=+ , 2=- , 3=* , 4=/) = ");
            type = kb.nextInt();
            
            System.out.println("\n\n");
            System.out.println("********** ATTENTION!!! **********");
            System.out.println(" now start the game!!!" + "\n");

            if (lv == 1)
            {
                if (type == 1)
                {
                    level1type1();
                }                
                else if ((type == 2)) 
                {
                    level1type2();
                }                 
                else if ((type == 3))                     
                {
                    level1type3();
                } 
                else 
                {
                    level1type4();
                }

            }
            
            else if (lv == 2)
                
            {
                if (type == 1)
                {
                    level2type1();                   
                }                
                else if ((type == 2))
                {
                    level2type2();
                }                 
                else if ((type == 3))
                {
                    level2type3();
                }                
                else                    
                {
                    level2type4();
                }
            } 
            else
            {
                if (type == 1) 
                {
                    level3type1();
                } 
                
                else if ((type == 2))
                {
                    level3type2();
                } 
                else if ((type == 3)) 
                {
                    level3type3();
                }
                else 
                {
                    level3type4();
                }
            }

            System.out.println("\n\n***** thank you for playing this game *****");
            

        }
        
        
    public static void level1type1()
    {
        Random rand = new Random();
        Scanner kb = new Scanner(System.in);
            
        int random1;
        int random2;
        int question;
        int ans;
        int mark = 0;
        int check;
                   do 
                   {
                        random1 = 1 + rand.nextInt(12);
                        random2 = 1 + rand.nextInt(12);

                        System.out.print(random1 + " + " + random2);
                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question = getquestion1(random1, random2);
                        if (ans == question)
                        {
                            mark = mark + 5;
                            System.out.println("--- your answer is correct, you can get the 5 point ---");
                        } 
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1)
                        {
                            System.out.println("\n");
                        }

                        if (check == 2)
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);    
                   
                   System.out.println("***** your this Lv total mark is " + mark + " *****");
    }
    
    public static void level1type2()
    {
        Random rand = new Random();
        Scanner kb = new Scanner(System.in);
            
        int random1;
        int random2;
        int question;
        int ans;
        int mark = 0;
        int check;
        
        do 
                    {
                        random1 = 1 + rand.nextInt(12);
                        random2 = 1 + rand.nextInt(12);

                        System.out.print(random1 + " - " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question = getquestion2(random1, random2);

                        if (ans == question)
                        {
                            mark = mark + 5;
                            System.out.println("--- your answer is correct, you can get the 5 point ---");
                        } 
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }
                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
        
        System.out.println("***** your this Lv total mark is " + mark + " *****");
    }
    
    public static void level1type3()
    {
        Random rand = new Random();
        Scanner kb = new Scanner(System.in);
            
        int random1;
        int random2;
        int question;
        int ans;
        int mark = 0;
        int check;
        
        do 
                    {
                        random1 = 1 + rand.nextInt(12);
                        random2 = 1 + rand.nextInt(12);

                        System.out.print(random1 + " * " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question = getquestion3(random1, random2);

                        if (ans == question)
                        {
                            mark = mark + 5;
                            System.out.println("--- your answer is correct, you can get the 5 point ---");
                        } 
                        else
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1)
                        {
                            System.out.println("\n");
                        }

                        if (check == 2)
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
        
        System.out.println("***** your this Lv total mark is " + mark + " *****");
    }
    
    public static void level1type4()
    {
        Random rand = new Random();
        Scanner kb = new Scanner(System.in);
            
        int random1;
        int random2;
        double question2;
        double ans;
        int mark = 0;
        int check;
        
        do
                    {
                        random1 = 1 + rand.nextInt(12);
                        random2 = 1 + rand.nextInt(12);

                        System.out.print(random1 + " / " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextDouble();

                        System.out.println();

                        question2 = getquestion4(random1, random2);

                        if (ans == question2) 
                        {
                            mark = mark + 5;
                            System.out.println("--- your answer is correct, you can get the 5 point ---");
                        } 
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1)
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
        System.out.println("***** your this Lv total mark is " + mark + " *****");
        
    }
    
    public static void level2type1()
            {
                Random rand = new Random();
                Scanner kb = new Scanner(System.in);
                
                int random1;
                int random2;
                int question;
                int ans;
                int mark = 0;
                int check;
        
                do 
                    {
                        random1 = 12 + rand.nextInt(39);
                        random2 = 12 + rand.nextInt(39);

                        System.out.print(random1 + " + " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question = getquestion1(random1, random2);

                        if (ans == question) 
                        {
                            mark = mark + 10;
                            System.out.println("--- your answer is correct, you can get the 10 point ---");
                        } 
                        
                        else
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                System.out.println("***** your this Lv total mark is " + mark + " *****");
            }
    
    public static void level2type2()
            {
                Random rand = new Random();
                Scanner kb = new Scanner(System.in);
                
                int random1;
                int random2;
                int question;
                int ans;
                int mark = 0;
                int check;
                
                do 
                    {
                        random1 = 12 + rand.nextInt(39);
                        random2 = 12 + rand.nextInt(39);

                        System.out.print(random1 + " - " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question = getquestion2(random1, random2);

                        if (ans == question)
                        {
                            mark = mark + 10;
                            System.out.println("--- your answer is correct, you can get the 10 point ---");
                        }
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                System.out.println("***** your this Lv total mark is " + mark + " *****");
            }
    
    public static void level2type3()
            {
                Random rand = new Random();
                Scanner kb = new Scanner(System.in);
                
                int random1;
                int random2;
                int question;
                int ans;
                int mark = 0;
                int check;
                
                do
                    {
                        random1 = 12 + rand.nextInt(39);
                        random2 = 12 + rand.nextInt(39);

                        System.out.print(random1 + " * " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question = getquestion3(random1, random2);

                        if (ans == question)
                        {
                            mark = mark + 10;
                            System.out.println("--- your answer is correct, you can get the 10 point ---");
                        }
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1)
                        {
                            System.out.println("\n");
                        }

                        if (check == 2)
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                System.out.println("***** your this Lv total mark is " + mark + " *****");
            }
    
    public static void level2type4()
            {
                Random rand = new Random();
                Scanner kb = new Scanner(System.in);
                
                int random1;
                int random2;
                double question2;
                double ans;
                int mark = 0;
                int check;
                
                do
                    {
                        random1 = 12 + rand.nextInt(39);
                        random2 = 12 + rand.nextInt(39);

                        System.out.print(random1 + " / " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextDouble();

                        System.out.println();

                        question2 = getquestion4(random1, random2);

                        if (ans == question2) 
                        {
                            mark = mark + 10;
                            System.out.println("--- your answer is correct, you can get the 10 point ---");
                        }
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                System.out.println("***** your this Lv total mark is " + mark + " *****");
            }
    
    public static void level3type1()
            {
                Random rand = new Random();
                Scanner kb = new Scanner(System.in);
                
                int random1;
                int random2;
                int question;
                int ans;
                int mark = 0;
                int check;
                
                do 
                    {
                        random1 = 50 + rand.nextInt(51);
                        random2 = 50 + rand.nextInt(51);

                        System.out.print(random1 + " + " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question = getquestion1(random1, random2);

                        if (ans == question)
                        {
                            mark = mark + 20;
                            System.out.println("--- your answer is correct, you can get the 20 point ---");
                        }
                        else
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2)
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                System.out.println("***** your this Lv total mark is " + mark + " *****");
            }
    
    public static void level3type2()
            {
                Random rand = new Random();
                Scanner kb = new Scanner(System.in);
                
                int random1;
                int random2;
                int question;
                int ans;
                int mark = 0;
                int check;
                
                do 
                    {
                        random1 = 50 + rand.nextInt(51);
                        random2 = 50 + rand.nextInt(51);

                        System.out.print(random1 + " - " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question = getquestion2(random1, random2);

                        if (ans == question) 
                        {
                            mark = mark + 20;
                            System.out.println("--- your answer is correct, you can get the 20 point ---");
                        } 
                        else
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1)
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                System.out.println("***** your this Lv total mark is " + mark + " *****");
            }
    
    public static void level3type3()
            {
                Random rand = new Random();
                Scanner kb = new Scanner(System.in);
                
                int random1;
                int random2;
                int question;
                int ans;
                int mark = 0;
                int check;
                
                do
                    {
                        random1 = 50 + rand.nextInt(51);
                        random2 = 50 + rand.nextInt(51);

                        System.out.print(random1 + " * " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextInt();

                        System.out.println();

                        question = getquestion3(random1, random2);

                        if (ans == question)
                        {
                            mark = mark + 20;
                            System.out.println("--- your answer is correct, you can get the 20 point ---");
                        } 
                        else
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                System.out.println("***** your this Lv total mark is " + mark + " *****");
            }
    
    public static void level3type4()
            {
                Random rand = new Random();
                Scanner kb = new Scanner(System.in);
                
                int random1;
                int random2;
                double question2;
                double ans;
                int mark = 0;
                int check;
                
                do 
                    {
                        random1 = 50 + rand.nextInt(51);
                        random2 = 50 + rand.nextInt(51);

                        System.out.print(random1 + " / " + random2);

                        System.out.print(" entre the answer = ");
                        ans = kb.nextDouble();

                        System.out.println();

                        question2 = getquestion4(random1, random2);

                        if (ans == question2) 
                        {
                            mark = mark + 20;
                            System.out.println("--- your answer is correct, you can get the 20 point ---");
                        }
                        else 
                        {
                            System.out.println("--- sorry, your answer is incorrect, you can not get the any point ---");
                        }

                        System.out.print("* Enter (1)continue, (2)check your mark, (3)stop this game = ");
                        check = kb.nextInt();

                        if (check == 1) 
                        {
                            System.out.println("\n");
                        }

                        if (check == 2) 
                        {
                            System.out.println("\nyour current mark = " + mark + "\n");
                        }

                    } while (check != 3);
                System.out.println("***** your this Lv total mark is " + mark + " *****");
            }
    

    public static int getquestion1(int num1, int num2) 
    {
        int question;
        question = num1 + num2;

        return question;
    }

    public static int getquestion2(int num1, int num2) 
    {
        int question;
        question = num1 - num2;

        return question;
    }

    public static int getquestion3(int num1, int num2) 
    {
        int question;
        question = num1 * num2;

        return question;
    }

    public static double getquestion4(int num1, int num2) 
    {
        double question2;
        question2 = (double) num1 / num2;

        return question2;
    }

}
