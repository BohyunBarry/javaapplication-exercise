/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week9;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;


/**
 *
 * @author user
 */
public class FinalCa 
{
     public static void main(String[] args) throws IOException 
    {
        Scanner kb = new Scanner(System.in);
        int choice;
        System.out.println("Welcome to play this game,please choose you want to play with the numbers generated either randomly OR read from a file.");
        String [] menu={"1.Maths Questions from File" , "2.Random Maths Question (with points)" , "3.finish"};
        choice=displayMenu(menu); 
        while(choice!=3)
        {
            if(choice==1)
            {
                playGameFileData();
            }
            else if(choice==2)
            {
                playGameRandom(); 
                
            }
            choice=displayMenu(menu); 
            
        }
        
        System.out.println("\n\n***** thank you for playing this game *****");
        
    }
     
     public static void playGameFileData() throws IOException 
    {
        File inputFile = new File("tryFile.txt");        
        Scanner input = new Scanner(inputFile);        
        int number1;
        int number2;
        String Symbol;
        int solution;
        int answer;
        int mark=0;
        boolean correct;
        
            System.out.println("********** ATTENTION!!! **********");
            System.out.println(" now start the game!!!" + "\n");
            
            while (input.hasNext()) 
            {
                number1 = input.nextInt();
                Symbol = input.next();
                number2 = input.nextInt();
                
                solution=calculate(number1,Symbol,number2);
                answer=displayanswer();
                if(solution==answer)
                {
                    correct=true;
                    mark=mark+5;
                }
                else
                {
                    correct=false;
                }
                System.out.println(correct + "\n");
            }
            System.out.println("\nyour total mark = "+ mark + "\n");
    }
        public static int calculate(int number1,String Symbol,int number2)
        {            
            int solution=0;
            
        switch (Symbol) {
            case "+":            
                solution = number1 + number2;
                System.out.print(number1 + " + " + number2 + " = ");
                break;
            case "-":            
                solution = number1 - number2;
                System.out.print(number1 + " - " + number2 + " = ");
                break;
            case "*":            
                solution = number1 * number2;
                System.out.print(number1 + " * " + number2 + " = ");
                break;
            case "/":            
                solution = number1 / number2;
                System.out.print(number1 + " / " + number2 + " = ");
                break;  
        }                 
        return solution;           
        }
        
         public static int displayanswer()
            {
                int answer;
                Scanner kb = new Scanner(System.in);
                answer = kb.nextInt();
            
              return answer;                  
            }
     
     public static void playGameRandom()
     {
        int choice=0;
        int level = 0;
        int type = 0;
        int count =0;
        int mark;
         while(choice!=5)
                { 
                    String [] menu1={"1.set level" , "2.set type" , "3.play game" ," 4.show point","5.finish"};
                    choice=displayMenu(menu1); 
                    if(choice==1)
                   {
                       level = setLevel();                      
                   }
                    else if(choice==2)
                   {
                       type = setType();
                    }
                    else if(choice==3)
                   {
                        count=playGame(level,type);
                    }
                    else if(choice==4)
                    {
                        mark = showPoint(count,level);
                        displayPoint(mark,count,level);
                    }

                }
     }
     
     public static int displayMenu(String[] option)
   {
      int choice;
      System.out.println(Arrays.toString(option));
      choice=getValidInt(0,(option.length - 1),"Enter choice = ");
      return choice;
       
   }
     public static int getValidInt(int min,int max, String prompt)
   {
        Scanner kb=new Scanner(System.in);
       int value;
       System.out.print(prompt);
       value=kb.nextInt();
       
       while(value<(min+1) || value>(max+1) )
       {
           System.out.println("Error-integer value not in correct range\n");
           System.out.print(prompt);
           value=kb.nextInt();
       }
       return value;
   }
      
        
        
        public static int setLevel()
        {            
                int level;
                
                String [] menulevel={"1.level 1" , "2.level 2" , "3.level 3"};
                level=displayMenu(menulevel); 
                  
                return level;
              
        }
        
        public static int setType()
        {   
            int type;
            
            String [] menuType={"1.+" , "2.-" , "3.*" , "4./"};
            type=displayMenu(menuType); 
           
            
            return type;

        }
        
        public static int displayQuestion(int type, int random1, int random2)
        {
            int question; 
            
             if(type==1)
            {
                 question = random1 + random2;
                 System.out.println(random1 + "+" + random2);
            }
            else if(type==2)
            {
                 question = random1 - random2;
                 System.out.println(random1 + "-" + random2);
            }
            else if(type==3)
            {
                 question = random1 * random2;
                 System.out.println(random1 + "*" + random2);
            }
            else
            {
                 question = random1 / random2;    
                 System.out.println(random1 + "/" + random2);
            } 
             
         return question;

        }
        
        public static int playGame(int level , int type)
        {
            Random rand = new Random();
            Scanner kb = new Scanner(System.in);
            int ans;
            int show;
            int question;
            int random1;
            int random2;
            int count=0;
            int choice;
            
//            System.out.println(level);
//            System.out.println(type);
             System.out.println( "\n\n********** ATTENTION!!! **********");
             System.out.println( " now start the game!!!" + "\n");
             
             do
             {
                if(level==1)
                {
                    random1 = 1 + rand.nextInt(12);
                    random2 = 1 + rand.nextInt(12);                                                            
                }
                else if(level==2)
                {
                    random1 = 12 + rand.nextInt(39);
                    random2 = 12 + rand.nextInt(39);                  
                }
                else
                {
                    random1 = 50 + rand.nextInt(51);
                    random2 = 50 + rand.nextInt(51);                   
                }
               question = displayQuestion(type,random1,random2);  
             
            System.out.print("entre the answer =  ");
            ans = kb.nextInt();
                       
            if(ans==question)
            {
                count=count+1;
                System.out.println("--- your answer is correct ---\n");
            }
            else
            {
                System.out.println("--- sorry, your answer is incorrect ---\n");
            }
            
            String [] menu2={"1.continue" , "2.check your mark" , "3.stop this game"};
            choice=displayMenu(menu2); 
            
            System.out.print("\n");
            
            if(choice==2)
            {
                showPoint(count,level);
            }
            
             }while(choice!=3);
             
             
         return count;           
        }
        
        
        public static int showPoint(int count , int level)
        {
            int mark=0;

            for(int i = 1 ; i<=count ; i++)
            {
                 if(level==1)
                {
                    mark = mark + 5;
                }
                else if(level==2)
                {
                    mark = mark + 10;
                }
                else
                {
                    mark = mark + 20;              
                }
            }
            System.out.println("your current mark = " +  mark  + "\n");
            
         return mark;          
        }

        public static void displayPoint(int mark,int count,int level)
        {
                System.out.println("level " + level + " : you got " + mark + "\n");                  
        }
}