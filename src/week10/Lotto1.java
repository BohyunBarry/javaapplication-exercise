/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week10;

/**
 *
 * @author D00171569
 */
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class Lotto1 {

    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);
        System.out.println("how many set do you want:");
        int set = input.nextInt();
        for (int i = 0; i < set; i++) 
        {
            int[] lotto = new int[6];
            fillRandomArray(lotto, 1, 45);
            Arrays.sort(lotto);
            displayArray(lotto, set);
        }
        double totalCost = calculateCost(set);
        System.out.printf("Total Cost: €%.2f", totalCost);

    }

    public static int getRandomNumber(int min, int max) 
    {
        Random random = new Random();
        int number = random.nextInt(max) + min;
        return number;
    }

    public static void fillRandomArray(int[] list, int min, int max) 
    
    {
        int number=0;
        for (int i = 0; i < list.length; i++) 
        {            
            do 
            {
                list[i] = getRandomNumber(min, max);
                
            } while (list[i] == number);
            number = list[i];            

        }
    }

    public static void displayArray(int[] list, int numberOfSet) 
    {
        System.out.println(Arrays.toString(list));
    }

    public static double calculateCost(int numberOfSet) 
    {
        double totalCost = numberOfSet * 1.5;
        return totalCost;
    }

}